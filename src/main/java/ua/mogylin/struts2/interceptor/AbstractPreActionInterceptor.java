package ua.mogylin.struts2.interceptor;

import com.opensymphony.xwork2.ActionInvocation;

/**
 * Abstract class for interceptor that should be executed only before action
 * processing.
 * 
 * @author oleksii.mogylin
 * 
 */
public abstract class AbstractPreActionInterceptor extends
AbstractSeparatedInterceptor {

	private static final long serialVersionUID = -8001768085658478901L;

	@Override
	protected final String interceptAfterAction(
			final ActionInvocation invocation)
					throws Exception {
		// Do nothing
		return null;
	}

	@Override
	protected final boolean shouldInterceptAfterAction(
			final ActionInvocation invocation, final String actionResult) {
		return false;
	}

	@Override
	protected abstract String interceptBeforeAction(ActionInvocation invocation)
			throws Exception;

}
