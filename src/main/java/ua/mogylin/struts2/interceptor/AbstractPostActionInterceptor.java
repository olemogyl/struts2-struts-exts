package ua.mogylin.struts2.interceptor;

import com.opensymphony.xwork2.ActionInvocation;

/**
 * Abstract class for interceptor that should be executed only after action has
 * been processed.
 * 
 * @author oleksii.mogylin
 * 
 */
public abstract class AbstractPostActionInterceptor extends
AbstractSeparatedInterceptor {

	private static final long serialVersionUID = 6285203854037361905L;

	@Override
	protected abstract String interceptAfterAction(ActionInvocation invocation)
			throws Exception;

	@Override
	protected final String interceptBeforeAction(
			final ActionInvocation invocation)
					throws Exception {
		// Do nothing and return no result.
		return null;
	}

	@Override
	protected final boolean shouldInterceptBeforeAction(
			final ActionInvocation invocation) {
		return false;
	}

}
