package ua.mogylin.struts2.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * The base class for interceptor that has separate methods for pre and post-
 * action processing code.
 * 
 * @author oleksii.mogylin
 * 
 */
public abstract class AbstractSeparatedInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -2814587766117179079L;

	@Override
	public final String intercept(final ActionInvocation invocation)
			throws Exception {
		String beforeActionResult = null;
		if (shouldInterceptBeforeAction(invocation)) {
			beforeActionResult = interceptBeforeAction(invocation);
		}
		if (beforeActionResult != null) {
			return beforeActionResult;
		}
		String actionResult = invocation.invoke();
		String afterActionResult = null;
		if (shouldInterceptAfterAction(invocation, actionResult)) {
			afterActionResult = interceptAfterAction(invocation);
		}
		return afterActionResult == null ? actionResult : afterActionResult;
	}

	protected boolean shouldInterceptBeforeAction(
			final ActionInvocation invocation) {
		return true;
	}

	protected boolean shouldInterceptAfterAction(
			final ActionInvocation invocation, final String actionResult) {
		return true;
	}

	protected abstract String interceptAfterAction(ActionInvocation invocation)
			throws Exception;

	protected abstract String interceptBeforeAction(ActionInvocation invocation)
			throws Exception;

}
